
/**
 * La class encode permet d'encoder un code bar avant son écriture
 *
 * @author Christophe Van Waesberghe
 * @version déc 2014
 */
public class Encode implements Encoder {

    private String message; //Le message à encoder
    private int compression; //Le type de compression (0 non, 1 Huffman)
    private Matrice matrice;
    /**
     * Cette méthode permet d'encoder un message
     *
     * @param message message à encoder
     */
    public Encode(String message) {
        //encode(message);
    }

    /**
     * Cette méthode permet d'encoder un message et de le compresser si désiré
     *
     * @param msg le message à encoder
     * @param compression le type de compression à appliquer (0 non, 1 Huffmanà
     * @return La valeur renvoyée contient une matrice de bits correspondant au
     * message msg, encodé en respectant la configuration de cet encodeur
     */
    public boolean[][] encode(String msg, int compression) {
        
        this.compression = compression;
        matrice = new Matrice();
        matrice = Huffman.compression(msg, compression);
        matrice.setConfiguration(Config.createConfiguration(setTaille(matrice, compression), 0, compression));
        matrice.setMatriceBoolean();
        boolean[][] matDeBool2 = matrice.getMatrice();
        String configuration = matrice.getConfiguration();
        int taille = (int) Math.pow(2, 5 + TableConversion.bin2dec(matrice.getConfiguration().substring(0, 3)));

        boolean[][] matDeBool = new boolean[taille][taille];

        int k = 0;
        int l = 0;

        for (int i = 1; i < taille; i++) {
            for (int j = 1; j < taille; j++) {
                matDeBool[i][j] = matDeBool2[i - 1][j - 1];
            }
        }
        setParity(matDeBool, taille);

        return matDeBool;
    }

    /**
     * Cette methode permet de définir la taille du code bar
     *
     * @param matrice la matrice de boolean
     * @param compression le type de compression
     * @return la taille formatée selont les spec du format CB2D (Ex 0=32,
     * 1=64,...)
     */
    public static int setTaille(Matrice matrice, int compression) {
        int taille;
        switch (compression) {
            case 0:
                taille = 16 + matrice.getSizeCompressed();
                break;
            case 1:
                taille = 16 + matrice.getSizeTable() + 16 + matrice.getSizeCompressed();
                break;
            default:
                taille = 16 + matrice.getSizeCompressed();
                break;
        }
        if (taille <= 937) {
            return 0;
        } else if (937 < taille && taille <= 3945) {
            return 1;
        } else if (3945 < taille && taille <= 16105) {
            return 2;
        } else if (16105 < taille && taille < 65001) {
            return 3;
        }
        return 3;

    }
    /**
     * Cette méthode permet de renvoyer la taille du code barr
     * @return la taille du string
     */
    public int getStringLength()
    {
        int taille = 0;
        switch (compression) {
            case 0:
                taille = 16 + matrice.getSizeCompressed();
                break;
            case 1:
                taille = 16 + matrice.getSizeTable() + 16 + matrice.getSizeCompressed();
                break;
            default:
                taille = 16 + matrice.getSizeCompressed();
                break;
        }
        return taille;
    }   
    /**
     * Cette méthode permet de définir le bit de parité
     *
     * @param matDeBool la matrice de boolean
     * @param taille la taille du code bar
     */
    public void setParity(boolean[][] matDeBool, int taille) {
        int count[] = {0, 0, 0};
        for (int i = 1; i < taille; i++) {
            count[0] = 0;
            count[1] = 0;
            for (int j = 1; j < taille; j++) {
                if (matDeBool[i][j] == true) {
                    count[0]++;
                }
                if (matDeBool[j][i] == true) {
                    count[1]++;
                }
            }
            if (count[0] % 2 != 0) {
                matDeBool[i][0] = true;
            }
            if (count[1] % 2 != 0) {
                matDeBool[0][i] = true;
            }
            if (matDeBool[0][i] == true) {
                count[2]++;
            }
        }
        if (count[2] % 2 != 0) {
            matDeBool[0][0] = true;
        }
    }
}
