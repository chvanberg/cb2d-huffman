
/**
 * La classe StringGestion contient des méthodes qui aggisent sur des chaines de
 * caractères.
 *
 * @author Christophe
 * @version 3 déc 2014
 */
public class StringGestion {

    /**
     * Cette méthode indique si un caractère à déja été vus précédement dans le
     * string
     *
     * @param string Une chaine de caractère.
     * @param caract Le caractère que l'on teste.
     * @param currentPosition La position actuelle dans le string.
     * @return true si le caractère à déjà été vus / false sinon.
     */
    public static boolean alreadySeen(String string, char caract, int currentPosition) {
        if (getOccurence(caract, string) > 1) {

            for (int i = 0; i < currentPosition; i++) {

                if (caract == string.charAt(i)) {

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Cette methode renvoie le nombre de fois qu'une lettre apparait dans une
     * chaine de caractère
     *
     * @param caract Le caractère à tester.
     * @param string La chaine de caractère dans laquelle on effectue le test.
     * @return Le nombre d'occurence de cette lettre dans le string.
     */
    public static int getOccurence(char caract, String string) {
        int nbOccurence = 0; // Le nombre d'occurence de du caractere.

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == caract) {
                nbOccurence++;
            }
        }
        return nbOccurence;
    }
}
