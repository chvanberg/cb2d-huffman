
/**
 * This class is used to manage the screen.
 *
 * @author Christophe Van Waesberghe (contact@chrisv.be)
 * @version V0.1 - Nov 2013
 */
public class UI
{
    private static final int MILIS = 5;     //MILIS is the delay time betwen a cract is show in caractByCaract
    private static final boolean DEBUG= false; //DEBUG disable some fonctionality when you're in production mode.
    /**
     * This method is used to give a beautiful style in your consol based software. 
     * It write your string caracter by caracter with a delay.
     * 
     * @param string string à afficher caractère par caractère
     */
    public static void caractByCaract(String string){
        for(int i=0; i<string.length();i++){
            System.out.print(string.charAt(i));
            //Waiting to show the next char.
            if(!DEBUG){
                try {
                    Thread.sleep(MILIS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method is used to give a beautiful style in your consol based software. 
     * It write your string caracter by caracter with a delay (add an \n at the end of the string).
     * 
     * @param string à afficher caractère par caractère
     * 
     */
    public static void caractByCaractln(String string){
        caractByCaract(string);
        System.out.print("\n");
    }

    /**
     * This method make a break in the execution while the user press any key to continue.
     */
    public static void pressAnyKey(){
        caractByCaract("\nPress any key to continue");
        try{System.in.read();}
        catch(Exception e){}
        clearConsole();
    }

    /**
     * This method is used to clear the console
     */
    public final static void clearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                System.out.print('\u000C');
                Runtime.getRuntime().exec("cls");
            }
            else
            {
                System.out.print('\u000C');
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e){}
    }
}
