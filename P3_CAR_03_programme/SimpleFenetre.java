import javax.swing.*;
import javax.swing.BorderFactory;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.*;

import barcode2d.*;
/**
 * made by Baptiste Lombard
 * Version 0.1
 */
public class SimpleFenetre extends JFrame implements ActionListener{
    private JPanel panel;
    private JButton bouton;
    private JButton bouton2;
    private JButton generate;
    private JButton goBack;
    private JLabel label;
    private JTextArea codebar;
    private JComboBox listeCB2D;
    private JLabel labelvoid;
    private JLabel labelCB;
    private JLabel saveAs;
    private JTextField saveText;
    private JLabel savePng;
    private JComboBox png;
    private JScrollPane scrollPane;
    private JButton maj;
    
    public SimpleFenetre(){
        super();
 
        build();//On initialise notre fenêtre
    }
 
    private void build(){
        setTitle("CB2D"); //On donne un titre à      l'application
        setSize(350,350); //On donne une taille à notre fenêtre
        setLocationRelativeTo(null); //On centre la fenêtre sur l'écran
        setResizable(true); //On permet la redimensionnement de la fenêtre
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer lors du clic sur la croix
        setContentPane(buildContentPane());
    }
    
    private JPanel buildContentPane(){
        panel = new JPanel();
        panel.setBackground(Color.white);
        
        label = new JLabel("Welcome in CodeBar Advance !");
        JLabel label2 = new JLabel("Made by Christophe VW, Alexandre D, Baptiste L.");
        panel.add(label);
        panel.add(label2);
        
        bouton = new JButton("Create a Codebar 2D");
        bouton.addActionListener(this);
        panel.add(bouton);
 
        bouton2 = new JButton("Read a Codebar 2D");
        bouton2.addActionListener(this);
        panel.add(bouton2);
        
        labelvoid = new JLabel("Write your text in this area:");
        labelvoid.setVisible(false);
        panel.add(labelvoid);
        
        labelCB= new JLabel("Choose a Codebar to read:");
        labelCB.setVisible(false);
        panel.add(labelCB);
        
        png = Combo();
        
        codebar = new JTextArea(7,25);
        codebar.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK), 
        BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        codebar.setVisible(false);
        panel.add(codebar);
        
        scrollPane = new JScrollPane(codebar);
        scrollPane.setVisible(false);
        panel.add(scrollPane);
        
        saveAs= new JLabel("Save As:");
        saveAs.setVisible(false);
        panel.add(saveAs);
        
        saveText = new JTextField(15);
        saveText.setVisible(false);
        panel.add(saveText);
        
        savePng= new JLabel(".png");
        savePng.setVisible(false);
        panel.add(savePng);
        
        generate = new JButton("Enter");
        generate.addActionListener(this);
        generate.setVisible(false);
        panel.add(generate);
        
        maj = new JButton();
        try {
            Image refresh = ImageIO.read(getClass().getResource("button_refresh/Button_Refresh_Icon_16.png"));
            maj.setIcon(new ImageIcon(refresh));
        } 
        catch (IOException ex) {
        }
        maj.addActionListener(this);
        maj.setVisible(false);
        panel.add(maj);
        
        init();
        return panel;
    }   
    
    public void actionPerformed(ActionEvent e) {
       Object source = e.getSource();
 
        if(source == bouton){
            generate.setVisible(true);
            labelvoid.setVisible(true);
            labelCB.setVisible(false);
            codebar.setVisible(true);
            png.setVisible(false);
            bouton.setForeground(Color.blue);
            bouton2.setForeground(Color.black);
            saveAs.setVisible(true);
            saveText.setVisible(true);
            savePng.setVisible(true);
            scrollPane.setVisible(true);
            maj.setVisible(false);
        } 
        else if(source == bouton2){
            labelvoid.setVisible(false);
            labelCB.setVisible(true);
            codebar.setVisible(false);
            png = Combo();
            png.setVisible(true);
            saveAs.setVisible(false);
            saveText.setVisible(false);
            savePng.setVisible(false);
            scrollPane.setVisible(false);
            generate.setVisible(true);
            maj.setVisible(true);
            bouton2.setForeground(Color.blue);
            bouton.setForeground(Color.black);
        }
        else if(source == maj) {
            png.setVisible(false);
            png = Combo();
            png.setVisible(true);
        }
        else if(source == generate) {
            if(png.isVisible()) {
                String filename = "codebar/" + (String)png.getSelectedItem();
                try {
                BufferedImage img = ImageIO.read(new File(filename));
                int width = img.getWidth();
                int height = img.getHeight();

                Config decodage = new Config(filename, width, height);
            } catch (IOException exc) {
                JOptionPane.showMessageDialog(null, "This file doesn't exist anymore.","error",JOptionPane.ERROR_MESSAGE);
            }
          }
            else {
                int answer = 1, n = 0; 
                BarCode2DWriter[] barcode= new BarCode2DWriter[2];
                BarCode2DData writer[] = new BarCode2DData[2];
                String msg = codebar.getText();
                //Check carrectère non voulus
                char currentChar;
                for(int i=0; i< msg.length()-1;i++){
                    if(0<((int) msg.charAt(i)) && ((int) msg.charAt(i))<127){
                        currentChar=msg.charAt(i);
                    }
                    else{
                        currentChar='?';
                        msg=msg.substring(0,i)+currentChar+msg.substring(i+1);
                    }
                }
                //fin de check
                
                Encode encode1 = new Encode(msg);
                boolean[][] mat = encode1.encode(msg, answer);
                WriterBarCode CB2D = new WriterBarCode(mat);
                writer[0] = CB2D;
                barcode[0] = new BarCode2DWriter(writer[0]);
                answer = 0;
                    
                if(mat.length < 33) {
                 
                    Encode encode2 = new Encode(msg);
                    mat = encode2.encode(msg, answer);
                    CB2D = new WriterBarCode(mat);
                    writer[1] = CB2D;
                    barcode[1] = new BarCode2DWriter(writer[1]);
                    
                    if(encode1.getStringLength() > encode2.getStringLength()) {
                        n = JOptionPane.showConfirmDialog(
                        null,
                        "Warning, the compression is not gainful \n Do you want to compress the barcode?",
                        "Compression",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                        if(n != JOptionPane.YES_OPTION) answer = 1;
                    }
                }
                String filename = "CB2D.png";
                n = 7;
                if(saveText.getText() != "") filename = "codebar/" + saveText.getText() + ".png";
                try {
                    
                    BarCode2DFrame page = new BarCode2DFrame(writer[answer], msg);
                    page.setLocation(200,200);
                    page.setSize(350,350);
                    
                    String[] listOfPng = ListOfPng();
                    for( int i = 0; i < listOfPng.length;i++) {
                        if(filename.substring(8).equals(listOfPng[i]))
                        { 
                            n = JOptionPane.showConfirmDialog(
                            null,
                            "The file you have selected already exists.\n Do you want to overwrite it?",
                            "Warning",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE);
                        }
                    }
                    
                    if(n == JOptionPane.YES_OPTION || n == 7) barcode[answer].drawBarCode2D(filename, mat.length, mat.length);
                    
                } catch (java.io.IOException f) {
    
                }
        }
        }
    }
    /**
     * Ajoute le dossier codebar s'il n'existe pas
     */
    public void init()
    {
        File barcode = new File("codebar"); 
        barcode.mkdirs(); 
    }
    /**
     * @return Un tableau de String
     */
    public String[] ListOfPng()
    {
        String path = "codebar"; 
 
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        String[] listOfPng = new String [listOfFiles.length];
             
       int num = 0;
                for (int i = 0; i < listOfFiles.length; i++) 
              {
             
                   if (listOfFiles[i].isFile()) 
                   {
                       files = listOfFiles[i].getName();
                       if (files.endsWith(".png") || files.endsWith(".PNG"))
                       {
                         listOfPng[num] = files;
                          num++;
                        }
                    }
              }
        String[] listOfPng2 = new String[num]; 
        for(int i = 0; i < num; i++) {
                listOfPng2[i] = listOfPng[i];
            }
        return listOfPng2; 
    }
    /**
     * @return instance de l'objet JComboBox
     */
    public JComboBox Combo()
    {
        
        String[] listOfPng = ListOfPng();
            Object[] elements = new Object[listOfPng.length];
            for(int i = 0; i < listOfPng.length; i++) {
                elements[i] = listOfPng[i];
            }
            listeCB2D = new JComboBox(elements);
            listeCB2D.setVisible(false);
            panel.add(listeCB2D);
            
            return listeCB2D;
       }
}
