
import java.util.*;

/**
 * La class huffman contient une série de méthode permettant la compression et
 * la décompression d'un texte
 *
 * @author Christophe Van Waesberghe pour P3_CAR_03
 * @version déc 2014
 */
public class Huffman {

    /**
     * Cette méthode permet de transformer un texte en une série de 0 et de 1 et
     * de le compresser si il le faut
     *
     * @param string le texte à traduire/compresser
     * @param compression 1 pour la compression de Huffman, 0 pour de L'ASCII
     * 7-bits
     * @return une instance de l'objet Matrice
     */
    public static Matrice compression(String string, int compression) {
        long startTime = System.currentTimeMillis();
        Matrice matrice = new Matrice();
        switch (compression) {

            case 0:
                matrice = new Matrice(null, TableConversion.getStringInBit(string, 7), string);
                break;
            case 1:
                Lettre[] tableauLettres = TableauLettres.create(string);
                List<Noeud> nodes = Noeud.create(tableauLettres);
                Noeud.doTheHuffmanTree(nodes);
                compress(nodes.get(0));
                String stringCompressed = toString(string, tableauLettres);
                String tableConversion = TableConversion.getStringInBit(TableConversion.printTable(tableauLettres), 7);
                matrice = new Matrice(tableConversion, stringCompressed, string);
                break;
        }
        System.out.println("Time for compression : "+((System.currentTimeMillis())-startTime));
        return matrice;
    }

    /**
     * Cette methode permet de traduire une série de 0 et de 1 vers un texte
     * lisible par l'homme, elle permet également de le décompresser si il l'est
     *
     * @param matrice l'instance de l'objet Matrice que l'on veux transformer
     * @param compression 1 si il y a une compression de Huffman, 0 si c'est de
     * l'ASCII 7-bits
     * @return le texte traduit et lisible par l'homme
     */
    public static String decompression(Matrice matrice, int compression) {
        long startTime = System.currentTimeMillis();
        String string = "";
        switch (compression) {

            case 0:
                string = decompressTableConversion(matrice.getStringCompressed());
                break;
            case 1:
                String tableConversion = decompressTableConversion(matrice.getTableConversion());
                String stringCompressed = matrice.getStringCompressed();

                Lettre[] tableauLettres = TableauLettres.createFromTable(tableConversion);
                List<Noeud> nodes = Noeud.create(tableauLettres);
                Noeud.doTheHuffmanTree(nodes);
                string = Huffman.decompress(stringCompressed, nodes);
                ;

                break;
        }
         System.out.println("Time for compression : "+((System.currentTimeMillis())-startTime));
        return string;
    }

    /**
     * Cette méthode permet d'attribuer le code compressé à chaque lettre
     *
     * @param node le noeud le plus lour de l'arbre de Huffman
     */
    public static void compress(Noeud node) {
        Lettre lettre = node.getLettre();
        if (!haveChild(node)) {

            node.getLettre().setCompressed(getTheCompressed(node));
            //node.getLettre().setCompressed((parent.getLettre().getCompressed()+node.getLettre().getCompressed()).substring();

            //(node.getLettre()).setCompressed(node.getLettre().getCompressed().substring(0,(node.getLettre().getCompressed().length()-1)));
        }
        if (haveRight(node)) {

            compress(node.getDroite());
        }
        if (haveLeft(node)) {

            compress(node.getGauche());
        }

    }

    /**
     * Cette méthode permet de retourner le code compressé d'un noeud
     *
     * @param node le noeud en question
     * @return le code de compression sous forme d'un string
     */
    public static String getTheCompressed(Noeud node) {
        String string = "";
        Noeud parent = node;

        do {
            string = parent.getLettre().getCompressed() + string;
            parent = parent.getParent();
        } while (haveParent(parent));
        return string;
    }

    /**
     * Cette methode permet de savoir si un noeud possède un enfant à "droite"
     *
     * @param node le noeud à tester
     * @return true si c'est le cas, false sinon
     */
    public static boolean haveRight(Noeud node) {
        if (node.getDroite() != null) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * Cette methode permet de savoir si un noeud possède un enfant à "gauche"
     *
     * @param node le noeud à tester
     * @return true si c'est le cas, false sinon
     */
    public static boolean haveLeft(Noeud node) {
        if (node.getGauche() != null) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * Cette méthode permet de savoir si le noeud possède un noeud parent
     *
     * @param node le noeud à tester
     * @return true si c'est le cas, false sinon
     */
    public static boolean haveParent(Noeud node) {
        try {
            if (node.getParent() != null) {
                return true;
            } else {
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }
    }

    /**
     * Cette méthode permet de savoir si un noeud possède au moins un enfant.
     *
     * @param node le noeud à tester
     * @return true si c'est le cas, false sinon
     */
    public static boolean haveChild(Noeud node) {
        if (node.getDroite() != null || node.getGauche() != null) {
            return true;
        } else {

            return false;
        }

    }

    /**
     * Cette méthode permet de retourner sous forme de string le texte compressé
     *
     * @param string le texte à compresser
     * @param tableauLettres la table de conversion
     * @return le texte compressé sous forme de 0 et de 1
     */
    public static String toString(String string, Lettre[] tableauLettres) {
        String stringCompressed = "";
        for (int i = 0; i < string.length(); i++) {
            stringCompressed += getCompression(string.charAt(i), tableauLettres);
        }
        return stringCompressed;
    }

    /**
     * Cette méthode permet de retourner le code compressé d'une lettre
     *
     * @param lettre la lettre en question
     * @param tableauLettres la table de conversion
     * @return le code compressé sous forme de string
     */
    public static String getCompression(char lettre, Lettre[] tableauLettres) {
        for (int i = 0; i < tableauLettres.length; i++) {
            if (lettre == tableauLettres[i].getLettre()) {
                return tableauLettres[i].getCompressed();
            }

        }
        return "Error";
    }

    /**
     * Cette méthode permet de décompresser une chaine de 0 et de 1
     *
     * @param stringCompressed le texte sous forme de 0 et de 1 (compressé)
     * @param nodes l'arbre de huffman
     * @return le string décompressé
     */
    public static String decompress(String stringCompressed, List<Noeud> nodes) {
        String stringDecompressed = "";
        String stringDecoupe = "";

        int curseur1 = 0;
        int curseur2 = 0;
        while (curseur2 <= stringCompressed.length()) {
            stringDecoupe = stringCompressed.substring(curseur1, curseur2);

            if (isLetter(stringDecoupe, nodes.get(0))) {

                stringDecompressed += getLetter(stringDecoupe, nodes.get(0));
                curseur1 = curseur2;

            }
            curseur2++;

        }
        return stringDecompressed;
    }

    /**
     * Cette méthode permet de décompresser la table de compression
     *
     * @param stringCompressed la table de compression
     * @return la table de compression formatée
     */
    public static String decompressTableConversion(String stringCompressed) {
        if (stringCompressed == null) {
            return "0";
        }
        String string = "";

        String stringDecoupe = "";
        int curseur1 = 0;
        int curseur2 = 7;
        while (curseur2 <= stringCompressed.length()) {
            stringDecoupe = stringCompressed.substring(curseur1, curseur2);
            int charCode = Integer.parseInt(stringDecoupe, 2);
            stringDecoupe = "";
            stringDecoupe += new Character(((char) charCode));

            string += stringDecoupe;
            curseur1 += 7;
            curseur2 += 7;

        }
        return string;
    }

    /**
     * Cette méthode permet de savoir si un série de 0 et de 1 représente une
     * lettre
     *
     * @param string le code à tester
     * @param node l'arbre de huffman
     * @return true si c'est le cas, false sinone
     */
    public static boolean isLetter(String string, Noeud node) {
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '1' && haveLeft(node)) {

                node = node.getDroite();
            } else if (string.charAt(i) == '0' && haveRight(node)) {
                node = node.getGauche();
            }
        }

        if (node.getLettre().getLettre() != '*') {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Cette méthode permet de retourner le caractère traduit par le code de
     * compression
     *
     * @param string le code de la lettre
     * @param node l'arbre de huffman
     * @return le caractère qui équivaut au code
     */
    public static char getLetter(String string, Noeud node) {
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '1' && haveRight(node)) {
                node = node.getDroite();
            } else if (string.charAt(i) == '0' && haveLeft(node)) {
                node = node.getGauche();
            }
        }

        return node.getLettre().getLettre();

    }

}
