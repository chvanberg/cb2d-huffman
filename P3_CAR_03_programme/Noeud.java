import java.util.*;

/**
 * La classe Noeud permet la création et l'utilisation d'objet de type Noeud..
 *
 * Il faut visualiser chaques Noeud comme un petit arbre avec deux fils et dans
 * certains cas un parent, le poids du noeud est la somme du poids de ses
 * enfants
 *
 * Exemple : Noeud : {*:poidsGauche + poidsDroite} Enfant Gauche : {a:2} Enfant
 * droite : {b:1}
 *
 * @author Christophe Van Waesberghe (Pour P3_CAR_03)
 * @version 3 déc 2014
 */
public class Noeud implements Comparable {

    /**
     * lettre contient un objet Lettre et donc un des caractère du string ainsi
     * que ses méta données.
     */
    protected Lettre lettre;
    /**
     * droite contient la référence vers le noeud "enfant", situé à sa "droite".
     */
    protected Noeud droite;
    /**
     * gauche contient la référence vers le noeud "enfant", situé à sa "gauche".
     */
    protected Noeud gauche;
    /**
     * parent contient la référence vers le noeud "parent"
     */
    protected Noeud parent;
    /**
     * poids contient le nombre de répétition de la lettre dans le string. Note
     * : Chaques nouveau noeud possède la somme du poids de ses enfants.
     */
    protected int poids = 1;

    /**
     * Constructeur ne prenant aucun paramètre.
     */
    public Noeud() {

    }

    /**
     * Constructeur prenant comme paramètre la lettre
     *
     * @param lettre la lettre en question
     */
    public Noeud(Lettre lettre) {
        this.lettre = lettre;
        this.poids = lettre.getProbability();
    }

    /**
     * Constructeur prenant comme paramètre : lettre, droite, gauche et poids
     *
     * @param lettre la lettre en question
     * @param droite référence vers le noeud enfant situé à droite
     * @param gauche référence vers le noeud enfant situé à gauche
     */
    public Noeud(Lettre lettre, Noeud droite, Noeud gauche) {
        this.lettre = lettre;
        this.droite = droite;
        this.gauche = gauche;
        this.poids = droite.getPoids() + gauche.getPoids();
    }

    /**
     * Cette méthode crée une list de Noeud à partir d'un tableau de lettre
     *
     * @param tableauLettres (voir tableauLettres.java)
     * @return une list de Noeud
     */
    public static List<Noeud> create(Lettre[] tableauLettres) {
        ArrayList<Noeud> nodes = new ArrayList<Noeud>();

        for (int index = 0; index < tableauLettres.length; index++) {
            Noeud temp = new Noeud(tableauLettres[index]);
            nodes.add(temp);
        }

        return nodes;
    }

    /**
     * Cette méthode réalise l'arbre de Huffman. Elle craie un noeud comprenent
     * pour enfant les deux noeud qui ont le poids le plus petit. Elle continue
     * jusqu'à ce qu'il ne reste plus qu'un noeud qui devient donc le sommet de
     * l'arbre et donc le poids est la longueur de la chaine de caractère de
     * base
     *
     * @param nodes Une list de Noeud
     *
     */
    public static void doTheHuffmanTree(List<Noeud> nodes) {
        Collections.sort(nodes);
        int niveauMax = ((Lettre) nodes.get(0).getLettre()).getStringLength();

        do {

            if (nodes.size() > 1) {

                nodes.add(new Noeud(new Lettre('*', nodes.get(0).getLettre().getStringLength()), nodes.get(0), nodes.get(1)));
                nodes.get(0).setParent(nodes.get(nodes.size() - 1));
                nodes.get(1).setParent(nodes.get(nodes.size() - 1));
                nodes.get(0).getLettre().setCompressed("1");
                nodes.get(1).getLettre().setCompressed("0");
                nodes.remove(0);

                nodes.remove(0);

            } else if (nodes.size() == 1) {

                nodes.get(0).getLettre().setCompressed("1");

            }

            Collections.sort(nodes);

        } while (((nodes.get(nodes.size() - 1).getPoids()) < niveauMax));

    }

    /**
     * Méthode setter pour définir le "parent" d'un noeud.
     *
     * @param parent Référence vers le noeud "parent"
     */
    public void setParent(Noeud parent) {
        this.parent = parent;
    }

    /**
     * Méthode getter pour récupérer la référence vers le "parent" d'un noeud
     *
     * @return La référence vers le "parent" d'un noeud.
     */
    public Noeud getParent() {
        return parent;
    }

    /**
     * Methode getter pour récupérer le caractère d'une lettre contenus dans un
     * noeud.
     *
     * @return le caractère de la lettre contenue dans le noeud
     */
    public char getChar() {
        return this.lettre.getLettre();
    }

    /**
     * Methode getter pour récupérer la lettre contenus dans le noeud
     *
     * @return la lettre contenue dans le noeud
     */
    public Lettre getLettre() {
        return this.lettre;
    }

    /**
     * Méthode getter pour récupérer la référence de l'enfant à droite
     *
     * @return La référence de l'enfant à droite.
     */
    public Noeud getDroite() {
        return this.droite;
    }

    /**
     * Méthode getter pour récupérer la référence de l'enfant à gauche
     *
     * @return La référence de l'enfant à gauche
     */
    public Noeud getGauche() {
        return this.gauche;
    }

    /**
     * Méthode getter pour récupérer le poids du noeud.
     *
     * @return Le poids du noeud.
     */
    public int getPoids() {
        return this.poids;
    }

    /**
     * Cette méthode configure le point de comparaison des noeud, le poids.
     *
     * @param objet à comparer
     * @return -1 si l'objet est arrière, 0 si ils sont en même position et 1 si
     * il est en avant.
     */
    @Override
    public int compareTo(Object objet) {
        int nombre1 = ((Noeud) objet).getPoids();
        int nombre2 = this.getPoids();
        if (nombre1 > nombre2) {
            return -1;
        } else if (nombre1 == nombre2) {
            return 0;
        } else {
            return 1;
        }

    }

    /**
     * Cette méthode permet d'afficher une List de Noeud.
     *
     * @param nodes une List de noeud.
     */
    public static void printNode(List<Noeud> nodes) {
        System.out.println("Lettre\tDroite\tGauche\tPoids");
        for (int i = 0; i < nodes.size(); i++) {

            System.out.print(
                nodes.get(i).getChar()
                + "\t");
            if (nodes.get(i).getDroite() != null) {
                System.out.print((nodes.get(i).getDroite()).getChar());
            } else {
                System.out.print("N/A");
            }
            System.out.print("\t");
            if (nodes.get(i).getGauche() != null) {
                System.out.print((nodes.get(i).getGauche()).getChar());
            } else {
                System.out.print("N/A");
            }
            System.out.println("\t" + nodes.get(i).getPoids());
        }
    }

    public static Lettre[] create(String string) {
        int length = string.length() + 1; //Longueur de la chaine
        Lettre[] tableauLettresTemp = new Lettre[length]; //On crée un tableau de la taille du string
        int nbLettres = 0; //Nombre de lettres diférentes du string

        //On parcourt le string
        for (int i = 0; i < length - 1; i++) {
            //Si la lettre n'as pas déjà été écrite dans le tableau
            if (!StringGestion.alreadySeen(string, string.charAt(i), i)) {
                //On remplis le tableau avec la lettre
                tableauLettresTemp[nbLettres] = new Lettre(string.charAt(i), StringGestion.getOccurence(string.charAt(i), string), length - 1);

                nbLettres++;
            }

        }

        //On crée un tableau de lettre qui à la taille du nombre de lettre diférentes.
        Lettre[] tableauLettres = new Lettre[nbLettres];

        //On le remplis
        for (int i = 0; i < nbLettres; i++) {
            tableauLettres[i] = tableauLettresTemp[i];
        }

        //On trie le tableau avant de le retourner
        return sort(tableauLettres);
    }

    /**
     * Cette méthode crée un tableau de lettres à partir d'une table de
     * conversion (Chaine de caractère formatée)
     *
     * @param tableConversion La table de conversion (Obtenue à l'exportation
     * d'un arbre de Huffman).
     * @return Un tableau de lettre trié (en fonction de leur fréquence
     * d'apparition dans le chaine).
     */
    public static Lettre[] createFromTable(String tableConversion) {
        Lettre[] lettre = new Lettre[127];
        int index = 0;
        int stringLength = 0;
        int nbLettres = 0;
        String string;

        int curseurLettre = 0;
        int previousLetttre = 0;
        while (curseurLettre < tableConversion.length()) {
            char currentChar = tableConversion.charAt(curseurLettre);

            if (((int) currentChar < 48 || 57 < (int) currentChar) && (int) currentChar != 126) {

                lettre[index] = new Lettre(currentChar);

                previousLetttre = curseurLettre + 1;
                nbLettres++;

            } else if ((int) currentChar == 126) {
                string = tableConversion.substring(previousLetttre, curseurLettre);
                stringLength += Integer.parseInt(string);

                lettre[index].setProbability(Integer.parseInt(string));
                string = "";
                index++;
            }
            curseurLettre++;
        }

        //On crée un tableau de lettre qui à la taille du nombre de lettre diférentes.
        Lettre[] tableauLettres = new Lettre[nbLettres];

        //On le remplis
        for (int i = 0; i < nbLettres; i++) {
            tableauLettres[i] = lettre[i];
            tableauLettres[i].setStringLength(stringLength);
        }

        return tableauLettres;
    }

    /**
     * Cette méthode permet de trier un tableau de lettres en fonction du nombre
     * de fois qu'une lettre apparait dans la chaine de caractère.
     *
     * @param tableauLettres Un tableau de Lettre.
     * @return Retourne un tableau de lettre trié.
     */
    public static Lettre[] sort(Lettre[] tableauLettres) {
        Lettre temp; // Une lettre temporaire pour le swap

        for (int currentChar = 0; currentChar < tableauLettres.length - 1; currentChar++) {
            for (int i = 0; i < tableauLettres.length - 1; i++) {
                if (tableauLettres[i].getProbability() > tableauLettres[i + 1].getProbability()) {
                    temp = tableauLettres[i + 1];
                    tableauLettres[i + 1] = tableauLettres[i];
                    tableauLettres[i] = temp;
                }
            }
        }

        return tableauLettres;
    }

    /**
     * Cette méthode permet d'afficher un tableau de lettre.
     *
     * @param tableauLettres Un tableau de Lettre
     */
    public static void toString(Lettre[] tableauLettres) {
        String string = "";
        System.out.println("Longueur de la chaine : " + tableauLettres[0].getStringLength());
        System.out.println("\nLettre\tOccurence(s)\tCode compressee");
        for (int i = 0; i < tableauLettres.length; i++) {
            if (tableauLettres[i].getLettre() == '\n') {
                string = "\\n";
            } else {
                string += tableauLettres[i].getLettre();
            }
            System.out.println("\"" + string + "\"\t" + tableauLettres[i].getProbability() + "\t\t" + tableauLettres[i].getCompressed());
            string = "";
        }

    }
    
}