
/**
 * L'interface décoder reprend les méthode utile au décodage d'un code bar
 *
 * @author P3_CAR_03
 * @version déc 2014
 */

public interface Decoder {

    /**
     * Cette méthode permet de décoder une matrice de boolean
     *
     * @param data != null data est une matrice carrée, de taille 32, 64, 128 ou
     * 256 data ne contient que des 0 et des 1
     * @return La valeur renvoyée contient le décodage de la matrice de bits
     * data décodée en respectant la configuration de ce décodeur Si la matrice
     * contient une erreur, celle-ci est corrigée
     */
    public String decode(boolean[][] data);

    /**
     * Cette methode renvoie la configuration du code bar
     *
     * @return La valeur renvoyée est true si data ne contient pas d'erreurs (la
     * parité de la matrice de bits est valide), et false sinon
     */
    public boolean getConfiguration();
}
