
/**
 * Décrivez votre interface Configuration ici.
 *
 * @author (P3_CAR_03)
 * @version (novembre 2014)
 */
public interface Configuration {

    /**
     * Cette méthode permet de retourner la taile d'un code barre
     * @return La valeur renvoyée contient la taille du code-barres
    */
    public int getSize();

    /**
     * Cet methode envoie le type du code bar
     * @return La valeur renvoyée contient le type de données du code-barres
    */
    public int getDataType();

    /**
     * Cette méthode renvoie si le code bar est compressé ou non
     * @return La valeur renvoyée contient le type de compression du code-barres
    *       non compresse - 1
    *       compresse - 0
    */
    public int getCompressionMode();
}
