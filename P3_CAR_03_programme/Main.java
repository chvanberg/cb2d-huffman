
import java.util.*;
import java.io.*;
import barcode2d.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * La classe main reprend toutes les méthode pour faire intéragir l'utilisateur
 * avec le programme
 *
 * @author P3_CAR_03
 * @version déc. 2014
 */
public class Main {

    /**
     * Méthode principale du projet
     *
     * @param args le nom du fichier à lire si il est passé en argument
     */
    public static void main(String[] args) {

        int answer = introduction();
        if (answer == 1) {
            UI.pressAnyKey();
            doYouWantToCompress();
        } else if (answer == 2) {
            try {
                BufferedImage img = ImageIO.read(new File("CB2D.png"));
                int width = img.getWidth();
                int height = img.getHeight();

                Config decodage = new Config("CB2D.png", width, height);
            } catch (IOException e) {
                System.err.println("Une erreur s'est produite lors de la premiere partie de lecture du Code Barre");
            }
        }

    }

    /**
     * Cette méthode permet de demander à l'utilisateur si il désire une
     * compression
     */
    public static void doYouWantToCompress() {
        boolean next = true;
        int answer = 0;
        UI.caractByCaractln(" Do you want to compress the barcode? (YES = 1 / NO = 0) ");
        while (next) {
            Scanner comp = new Scanner(System.in);
            try {
                answer = comp.nextInt();
                next = false;
            } catch (java.util.NoSuchElementException e) { //attrape les erreurs si 
                next = true;                            //l'utilisateur n'a pas entré un nombre.
            }
            if (answer > 1 || answer < 0) // le nombre doit etre compris entre 0 et 1.
            {
                next = true;
            }
            if (next) {
                UI.caractByCaractln("Please enter a valid number");
            }
        }
        String message = "";
        next = true;
        UI.caractByCaractln(" Enter your message ");
        while (next) {
            Scanner aEncoder = new Scanner(System.in);
            message = aEncoder.nextLine();
            if (message == null) { //attrape les erreurs si 
                next = true;                            //l'utilisateur n'a pas entré un nombre.
            } else {
                next = false;
            }
            message += " ";
        }
        //Appelez la méthode pour encoder des textes
        Encode encode2 = new Encode(message);
        boolean[][] mat = encode2.encode(message, answer);
        WriterBarCode CB2D = new WriterBarCode(mat);
        BarCode2DData writer = CB2D;
        new BarCode2DFrame(writer, message);
        BarCode2DWriter barCode = new BarCode2DWriter(writer);
        try {
            barCode.drawBarCode2D("CB2D.png", mat.length, mat.length);

        } catch (java.io.IOException e) {

        }
            //decodage !!
        //Config decodage = new Config("CB2D.png", 32, 32);

    }

    /**
     * Cette méthode permet d'afficher l'introduction du programme et lui
     * demander quoi faire
     *
     * @return le choix de l'utilisateur
     */
    public static int introduction() {
        boolean next = true;
        int answer = 0;
        UI.caractByCaractln("Welcome in CodeBar Advance !");
        UI.caractByCaractln("Made by P3_CAR_03, Christophe VW, Alexandre D, Baptiste L.\n");
        UI.caractByCaractln("-----");
        UI.caractByCaractln(" [1] : Create a QR code");
        UI.caractByCaractln(" [2] : Read a QR code");
        UI.caractByCaractln(" [3] : Exit");
        while (next) {
            Scanner intro = new Scanner(System.in);
            try {
                answer = intro.nextInt();
                next = false;
            } catch (java.util.NoSuchElementException e) { //attrape les erreurs si 
                next = true;                            //l'utilisateur n'a pas entré un nombre.
            }
            if (answer > 3 || answer < 1) // le nombre doit etre compris entre 1 et 2.
            {
                next = true;
            }
            if (next) {
                UI.caractByCaractln("Please enter a valid number");
            }

            UI.caractByCaractln(" You chose the number " + answer);
        }

        return answer;
    }

}
