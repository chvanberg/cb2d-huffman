
/**
 * Décrivez votre classe tableConversion ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class TableConversion {

    public static String printTable(Lettre[] tableauLettres) {
        String string = "";

        for (Lettre tableauLettre : tableauLettres) {
            string += tableauLettre.getLettre();
            string += tableauLettre.getProbability();
            string += (char) 6;
        }

        return string;
    }

    /**
     * Cette méthode permet de traduire un texte en une version binaire, les
     * caractèes sont encodé suivant la taille définir par radius
     *
     * @param s le texte à traduire
     * @param radius la taille de chaque lettre encodé (Ex 7 : 0000101, 8 :
     * 00000101)
     * @return le string encodé en binaire
     */
    public static String getStringInBit(String s, int radius) {
        String string = "";
        for (int i = 0; i < s.length(); i++) {
            String temp;
            temp = Integer.toBinaryString(s.charAt(i));
            for (int j = temp.length(); j < radius; j++) {
                temp = "0" + temp;
            }
            
            string += temp;

        }
        return string;
    }

    /**
     * Cette méthode permet de retourner le binaire d'un nombre
     *
     * http://openclassrooms.com/forum/sujet/java-s-cool-convertir-entier-naturel-en-binaire
     * adapté par Christophe Van Waesberghe.
     * @param decimal le chiffre à convertir
     * @param cut à quel moment on coupe le string pour obtenir la taille voulus
     * @return la version binaire d'un chiffre encoder sur un string
     */
    public static String getBin(int decimal, int cut) {

        String result = "";

        for (int i = 0; i < 8; i++) {

            if (decimal - Math.pow(2, 7 - i) > 0) {

                result += "1";
                decimal -= Math.pow(2, 7 - i);

            } else if (decimal - Math.pow(2, 7 - i) == 0) {

                result += "1";
                decimal = 0;

            } //Cette condition n'est pas obligatoire, le tableau de byte est rempli de 0 à la base.
            else {

                result += "0";

            }

        }
        return result.substring(8 - cut);

    }

    /**
     * Cette méthode permet de convertir un nombre en binaire vers sa notation
     * décimal
     *
     * @source
     * http://openclassrooms.com/forum/sujet/conversion-binaire-gt-decimal-20968
     * @param bin le chiffre encodé en binaire
     * @return le décimal traduit
     */
    static int bin2dec(String bin) //Binaire >> Decimal
    {
        int dec = 0;
        int bit;

        for (int i = bin.length(), j = 0; i > 0; i--, j++) {
            bit = Character.getNumericValue(bin.charAt(i - 1));
            dec += bit * Math.pow(2, j);
        }

        return dec;
    }
}
