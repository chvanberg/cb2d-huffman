/**
 * Cette méthode permet de creer des objet de Type Matrice utilisé pour facilité la création de code bar et leur compression
 * 
 * @author Christophe Van Waesberghe pour P3_CAR_03
 * @version Déc 2014
 */
public class Matrice
{
    private boolean[][] matrice; //La matrice de bits
    private String configuration; //La configuration du code bar
    private String tableConversion; //La table de conversion pour la compression/decompression
    private String stringCompressed; //La chaine de caractere (compressee ou non)
    private String string; //Le string encodé normalement
    private int sizeTable; //La taille de la table de compression
    private int sizeCompressed; //La taille du string compressé
    private int sizeTableAndCompressed; //La taille de la table + celle du string compressé
    private int sizeString; //La taille du string
    private int size; //La taille du code bar
    
    
    
    /**
     * Constructeur vide d'une Matrice
     */
    public Matrice(){
        
    }
    /**
     * Cette méthode permet de créer une instance de l'objet matrice avec un tableau de boolean
     * @param matrice tableau de boolean
     */
    public Matrice(boolean[][] matrice){
        this.matrice=matrice;
    }
   
    /**
     * Cette méthode permet de définir la matrice de boolean à partir d'est autres informations de la matrice
     */
    public void setMatriceBoolean(){
        String matriceEncode = this.getConfiguration();
        if(tableConversion != null) {
            matriceEncode += "000000";
            if(this.getTableConversion()!=""){
             matriceEncode+=tableConversion+"1111111111111111";   
            }
        }
        matriceEncode+=stringCompressed+"1111111111111111";
        //System.out.println("mat encode " + matriceEncode);
        int taille=(int)Math.pow(2,5+TableConversion.bin2dec(this.getConfiguration().substring(0,3)))-1;
        //System.out.println("taille" + taille);
        this.matrice= new boolean[taille][taille];
        int i=0;
        for(int y=0; y<taille;y++){
            for(int x=0; x<taille;x++){
                if(i<matriceEncode.length()){
                    if(matriceEncode.charAt(i)=='1'){
                         matrice[y][x]=true;
                    }
                    else{
                        matrice[y][x]=false;
                    }
                
            }
             i++;
            }
           
        }
        
    }
    /**
     * Cette méthode permet de définir la matrice de boolean
     * @param matrice  matrice de boolean
     */
    public void setMatriceBoolean(boolean[][] matrice){
        this.matrice=matrice;
    }
    
    /**
     * Contructeur d'une matrice comprenant la configuration, la table de conversion et le string
     * @param tableConversion la table de conversion compressée
     * @param stringCompressed le string compressé
     * @param string le texte
     */
    public Matrice(String tableConversion,String stringCompressed ,String string) {
        this.tableConversion=tableConversion;
        this.stringCompressed=stringCompressed;
        this.string=string;
        this.sizeString=string.length();
        if(tableConversion!=null){
        this.sizeTable=tableConversion.length();
    }
        this.sizeCompressed=stringCompressed.length();
        this.size=TableConversion.getStringInBit(string, 8).length();
        this.sizeTableAndCompressed=sizeTable+sizeCompressed;
    }
    
    /**
     * Cette méthode renvoie la taille totale prise par le code bar
     * @return la taille totale prise par le code bar
     */
    public int getTotalSize(){
        int totalSize=0;
        totalSize+=3;
        totalSize+=4;
        totalSize+=3;
        totalSize+=6;
        if(getTableConversion()!=null){
            totalSize+=getSizeTable()+8;
            totalSize+=getSizeCompressed();
        }
        else{
            totalSize+=getSizeString();
        }
        return totalSize;
    }
    /**
     * Cette méthode permet de définir la taille du code bar
     * @param taille 32,64,..
     * @return 0
     */
    public int[] setSize(int taille)
    {
        int a = 5;
        int[] size = {0,0,0,0}; 
        while(true)
        {
            if(taille < Math.pow(Math.pow(2,a)-1,2))
            {
                if(a > 8) { 
                    size[0] = -1; 
                    return size;
                }
                size[3] = (int)Math.pow(2,a);
                a =  (int)(Math.log((int)Math.pow(2,a)/32)/Math.log(2));
                for(int i = 7; i >= 0; i--)
                {
                    if(a >= Math.pow(2,i))
                    {
                       size[2-i] = 1;
                       a -= (int)Math.pow(2,i);
                    }
                    if(a==0)
                    {
                        return size;
                    }
                }
            }
            a++;
        }
    }
    
    /**
     * Methode setter pour définir la matrice
     * @param matrice la matrice de boolean
     */
    public void setMatrice(boolean[][] matrice){
        this.matrice=matrice;
    }
    /**
     * Cette méthode permet de définir le string compressé
     * @param stringCompressed le string compressé et traduit
     */
    public void setStringCompressed(String stringCompressed){
        this.stringCompressed=stringCompressed;
        this.sizeCompressed=stringCompressed.length();
    }
    
    /**
     * Cette méthode permet d'obtenir la taille de la table de compression
     * @return la taille de la table de compression
     */
    public int getSizeTable(){
        return sizeTable;
    }
    /**
     * Cette méthode permet d'obtenir la taille du string
     * @return la taille du string
     */
    public int getSizeString(){
        return sizeString;
    }
    /**
     * Cette méthode permet d'obtenir la taille du string compressé
     * @return la taille du string compressé
     */
    public int getSizeCompressed(){
        return sizeCompressed;
    }
    /**
     * Methode setter pour définir la configuration
     * @param configuration le string comprenant la configuration
     */
    public void setConfiguration(String configuration){
        this.configuration=configuration;
        
    }
   /**
    * Methode setter pour définir la table de compression
    * @param tableConversion la table de conversion
    */
    public void setTableConversion(String tableConversion){
        this.tableConversion=tableConversion;
        sizeTable=tableConversion.length();
    }
    /**
     * Methode setter pour définir le string
     * @param string à définir
     */
    public void setString(String string){
        this.string=string;
    }
    /**
     * Methode getter pour obtenir la matrice
     * @return matrice de boolean
     */
    public boolean[][] getMatrice(){
        return matrice;
    }
    /**
     * Methode getter pour obtenir la configuration
     * @return la configuration
     */
    public String getConfiguration(){
        return configuration;
      
    }
    /**
     * Methode getter pour obtenir la table de conversion
     * @return table de conversion
     */
    public String getTableConversion(){
        return tableConversion;
    }
    /**
     * Methode getter pour obtenir le string
     * @return le string
     */
    public String getString(){
        return string;
    }
    /**
     * Cette méthode permet d'obtenir le string compressé
     * @return string compressé
     */
    public String getStringCompressed(){
        return stringCompressed;
    }
    
}
