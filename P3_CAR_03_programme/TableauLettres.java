/**
 * Cette classe permet de créer et de gérer des tableau de Lettre
 *
 * @author Christophe Van Waesberghe
 * @version déc 2014
 */
public class TableauLettres
{

    /**
     * Cette méthode permet de créer un tableau de lettre à partir d'un string
     * @param string le string à analyser
     * @return un tableau d'objet Lettre
     */
    public static Lettre[] create(String string) {
        int length = string.length() + 1; //Longueur de la chaine
        Lettre[] tableauLettresTemp = new Lettre[length]; //On crée un tableau de la taille du string
        int nbLettres = 0; //Nombre de lettres diférentes du string
        
        //On parcourt le string
        
        
        for (int i = 0; i < length - 1; i++) {
            //Si la lettre n'as pas déjà été écrite dans le tableau
            
            
            
            if (!StringGestion.alreadySeen(string, string.charAt(i) , i)) {
                //On remplis le tableau avec la lettre
                tableauLettresTemp[nbLettres] = new Lettre(string.charAt(i), StringGestion.getOccurence(string.charAt(i), string), length - 1);

                nbLettres++;
            }

        }

        //On crée un tableau de lettre qui à la taille du nombre de lettre diférentes.
        Lettre[] tableauLettres = new Lettre[nbLettres];

        //On le remplis
        for (int i = 0; i < nbLettres; i++) {
            tableauLettres[i] = tableauLettresTemp[i];
        }

        //On trie le tableau avant de le retourner
        return sort(tableauLettres);
    }

    /**
     * Cette méthode crée un tableau de lettres à partir d'une table de
     * conversion (Chaine de caractère formatée)
     *
     * @param tableConversion La table de conversion (Obtenue à l'exportation
     * d'un arbre de Huffman).
     * @return Un tableau de lettre trié (en fonction de leur fréquence
     * d'apparition dans le chaine).
     */
    public static Lettre[] createFromTable(String tableConversion) {
        Lettre[] lettre = new Lettre[127];
        int index = 0;
        int stringLength = 0;
        int nbLettres = 0;
        String string;

        int curseurLettre = 0;
        int previousLetttre = 0;
        while (curseurLettre < tableConversion.length()) {
            char currentChar = tableConversion.charAt(curseurLettre);

            if (((int) currentChar < 48 || 57 < (int) currentChar) && (int) currentChar != 6) {

                lettre[index] = new Lettre(currentChar);

                previousLetttre = curseurLettre + 1;
                nbLettres++;

            } else if ((int) currentChar == 6) {
                string = tableConversion.substring(previousLetttre, curseurLettre);
                stringLength += Integer.parseInt(string);

                lettre[index].setProbability(Integer.parseInt(string));
                string = "";
                index++;
            }
            curseurLettre++;
        }

        //On crée un tableau de lettre qui à la taille du nombre de lettre diférentes.
        Lettre[] tableauLettres = new Lettre[nbLettres];

        //On le remplis
        for (int i = 0; i < nbLettres; i++) {
            tableauLettres[i] = lettre[i];
            tableauLettres[i].setStringLength(stringLength);
        }

        return tableauLettres;
    }

    /**
     * Cette méthode permet de trier un tableau de lettres en fonction du nombre
     * de fois qu'une lettre apparait dans la chaine de caractère.
     *
     * @param tableauLettres Un tableau de Lettre.
     * @return Retourne un tableau de lettre trié.
     */
    public static Lettre[] sort(Lettre[] tableauLettres) {
        Lettre temp; // Une lettre temporaire pour le swap

        for (int currentChar = 0; currentChar < tableauLettres.length - 1; currentChar++) {
            for (int i = 0; i < tableauLettres.length - 1; i++) {
                if (tableauLettres[i].getProbability() > tableauLettres[i + 1].getProbability()) {
                    temp = tableauLettres[i + 1];
                    tableauLettres[i + 1] = tableauLettres[i];
                    tableauLettres[i] = temp;
                }
            }
        }

        return tableauLettres;
    }

    /**
     * Cette méthode permet d'afficher un tableau de lettre.
     *
     * @param tableauLettres Un tableau de Lettre
     */
    public static void toString(Lettre[] tableauLettres) {
        String string = "";
        System.out.println("Longueur de la chaine : " + tableauLettres[0].getStringLength());
        System.out.println("\nLettre\tOccurence(s)\tCode compressee");
        for (int i = 0; i < tableauLettres.length; i++) {
            if (tableauLettres[i].getLettre() == '\n') {
                string = "\\n";
            } else {
                string += tableauLettres[i].getLettre();
            }
            System.out.println("\"" + string + "\"\t" + tableauLettres[i].getProbability() + "\t\t" + tableauLettres[i].getCompressed());
            string = "";
        }

    }

    /**
     * Cette méthode permet de commencer la création d'un arbre de huffman
     * @return un tableau de Lettre
     */
    public static Lettre[] createTableConversion(){
        final int startIndex=32;
        final int endIndex=127;
        final int borne=endIndex-startIndex;
        final int nodeStep=(int) '~';
        final int nodeSeparator=(int) '|';
        int index;
        Lettre[] tableauLettres = new Lettre[borne];
        for(int i=0;i<borne;i++){
            index=i+startIndex;
            tableauLettres[i]= new Lettre((char)index);

            if(index==nodeStep || index==nodeSeparator){
                tableauLettres[i].setProbability(10000);
            }
            else if(97<=index && index<=122){
                tableauLettres[i].setProbability(5000);
            }
            else if(65<=index && index<=90){
                tableauLettres[i].setProbability(1000);
            }

            else{
                tableauLettres[i].setProbability(1);
            }
            //+98+975+600
            tableauLettres[i].setStringLength(borne+19998+124975+24975);
        }
        sort(tableauLettres);
        return tableauLettres;
    }
}
