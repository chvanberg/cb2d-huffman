import barcode2d.BarCode2DReader;
import barcode2d.BarCode2DFrame;

import java.io.IOException;
/**
 * La classe configuration possède une série de méhode permetant de créer, lire et modifier la configuration du code barre
 * 
 * @author P3_CAR_03
 * @version dec 2014
 */
public class Config implements Configuration
{
    private String filename; //The name of the file (with it extension)
    private BarCode2DReader reader = new BarCode2DReader(); //Création d'une nouvelle instance de BarCode2DReader
    
    /**
     * Constructor for objects of class Config
     * 
     * @param filename //Le nom du fichier et son extension
     * @param tailleX //La taille du code bar (Longueur)
     * @param tailleY //La taille du code bar (hauteur)
     */
    public Config(String filename, int tailleX, int tailleY)
    {
        try
        {
            reader.loadBarCode2D (filename, tailleX, tailleY);
            boolean[][] matDeBits = new boolean[tailleX][tailleY];
            for(int i = 0; i < tailleX; i++) {
                for(int j = 0; j < tailleY; j++) {
                    matDeBits[i][j] = reader.getBarCodeData().getValue(i,j);
                }
            }
            Decode decodage = new Decode(matDeBits);
            decodage.ControlAndCorrection(matDeBits);
            Matrice decodee = decodage.decodeString(decodage.decode(matDeBits));
            reader.loadBarCode2D (filename, tailleX, tailleY);
            BarCode2DFrame page = new BarCode2DFrame (reader.getBarCodeData(), Huffman.decompression(decodee, Integer.parseInt(decodee.getConfiguration().substring(9,10))));
            page.setLocation(200,200);
            page.setSize(350,350);
        }
        catch (IOException exception)
        {
            System.err.println ("Une erreur s'est produite lors de la lecture du fichier");
        }
    }
    
    /**
     * Cette méthode permet de retourner la taile d'un code barre
     * @return La valeur renvoyée contient la taille du code-barres
    */
    public int getSize()
    {
        int size = 0;
        for(int i = 1; i < 4; i++)
        {
            if(reader.getBarCodeData().getValue(1,i)==true)
            {
                size = size + (int)Math.pow(2,3-i);
            }
        }
        return (int)Math.pow(2,5+size);
    }
    
    /**
     * Cet methode envoie le type du code bar
     * @return La valeur renvoyée contient le type de données du code-barres
    */
    public int getDataType()
    {
        int type = 0;
        for(int i = 4; i < 8; i++)
        {
            if(reader.getBarCodeData().getValue(1,i)==true)
            {
                type = type + (int)Math.pow(2,3-i);
            }
        }
        return type;
    }
    
    /**
     * Cette méthode renvoie si le code bar est compressé ou non
     * @return La valeur renvoyée contient le type de compression du code-barres
    *       non compresse - 1
    *       compresse - 0
    */
    public int getCompressionMode()
    {
        int compression = 0;
        for(int i = 8; i < 11; i++)
        {
            if(reader.getBarCodeData().getValue(1,i)==true)
            {
                compression = compression + (int)Math.pow(2,3-i);
            }
        }
        return compression;
    }
    
    /**
     * Cette méthode est utilisée pour créer une configuration suivant les spécificité du codebar2D 
     * @param taille la taille du code barre
     * @param type de donnée
     * @param compression 1 si elle est activée, 0 sinon
     * @return un string de 0 et de 1 formaté selont les spec du CB2D
     */
    public static String createConfiguration(int taille, int type, int compression){
        String configuration="";
        configuration+=TableConversion.getBin(taille,3)+TableConversion.getBin(type,4)+TableConversion.getBin(compression,3);
        
        return configuration;
    }
}
