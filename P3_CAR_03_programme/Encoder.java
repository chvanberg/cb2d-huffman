/**
 * Décrivez votre interface Encoder ici.
 *
 * @author  P3_CAR_03
 * @version novembre 2014
 */

public interface Encoder
{
    /**
     * Cette méthode permet d'encoder un message
     *
     * @param msg le message à encoder
     * @param compression le type de compression à appliquer
     * @return matrice de boolean
     */
    public boolean[][] encode (String msg, int compression);
    
}
