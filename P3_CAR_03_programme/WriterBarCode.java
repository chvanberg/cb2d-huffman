
import java.io.*;
import barcode2d.*;

/**
 * 
 */
public class WriterBarCode implements BarCode2DData
{
    private boolean[][] mat;
    
    /**
     * Constructeur
     * 
     * @param mat matrice de boolean
     */
    public WriterBarCode(boolean[][] mat) {
        this.mat = mat;
        
    }
    
    /**
     * Cette méthode permet de retourner la matrice boolean
     * @return matrice de boolean
     */
    public boolean[][] getData() {
        return mat;
    }
    
    /**
     * Cette methode getter return la hauteur
     * @return la hauteur
     */
    public int getHeight() {
        return mat.length;
    }
    
    /**
     * Cette methode getter return la longueur
     * @return la longueur
     */
    public int getWidth() {
        return mat[0].length;
    }
    
    /**
     * Cette méthode renvoie la valeur dans la matrice de boolean
     * @param i la colone
     * @param j la ligne
     * @return la valeur de la matrice à l'endroit donné
     */
    public boolean getValue(int i, int j) {
        return mat[i][j];
    }
    
}
