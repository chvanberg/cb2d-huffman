
/**
 * La classe decode contient toutes les méthodes utile au décodage des code bar
 *
 * @author P3_CAR_03
 * @version Dec 2014
 */
public class Decode implements Decoder {

    private boolean[][] data; //La matrice de boolean
    private static Matrice matrice = new Matrice(); //Crée une instance de l'objet matrice

    /**
     * Cette methode permet de décoder un code bar
     *
     * @param data matrice de boolean
     */
    public Decode(boolean[][] data) {
        this.data = data;
    }

    /**
     * Cette méthode permet de décoder un code bar
     *
     * @param data matrice de boolean
     * @return le string contenant le code bar sans les bits de parité
     */
    public String decode(boolean[][] data) {
        String decodage = "";
        boolean[][] matDeBool = data;
        int comp = 0;
        if (data[1][10]) {
            comp = 1;
        }
        //matrice.setConfiguration(Config.createConfiguration(matDeBool.length, 0, comp));
        for (int i = 1; i < matDeBool.length; i++) {
            for (int j = 1; j < matDeBool.length; j++) {
                if (matDeBool[i][j]) {
                    decodage += '1';
                } else {
                    decodage += '0';
                }
            }
        }
        matrice.setConfiguration(decodage.substring(0,10));
        return decodage;
    }

    /**
     * Cette méthode permet de décoder là série de 0 et de 1 du code bar afin de
     * séparer les diférentes informations
     *
     * @param string chaine de 0 et de 1 du code bar à décoder
     * @return une instance de l'objet Matrice remplie à partir du string
     */
    public static Matrice decodeString(String string) {
        int marqueurTableFin = 16;
        int marqueurFin = 16;
        int j = 0;
        String marqueur = "";
        matrice.setConfiguration(string.substring(0, 10));
        int comp = Character.getNumericValue(matrice.getConfiguration().charAt(matrice.getConfiguration().length() - 1));
        if ( comp == 1 ) {
            for (int i = 16; !(marqueur.equals("1111111111111111")) || i == string.length(); i++) {

                if (string.charAt(i) == '1') {
                    marqueur += "1";
                } else {
                    marqueur = "";
                }
                marqueurTableFin = i;
            }
            marqueur = "";

            matrice.setTableConversion(string.substring(16, marqueurTableFin - 15));
            marqueurFin = marqueurTableFin;
        }
        else {
            marqueurTableFin = 10;
        }

        for (int i = marqueurTableFin; !(marqueur.equals("1111111111111111")) && i != string.length(); i++) {

            if (string.charAt(i) == '1') {
                marqueur += "1";
            } else {
                marqueur = "";
            }
            marqueurFin = i;
        }
        for (int i = marqueurFin; string.charAt(i) == '1' || i == string.length(); i++) {
            j++;
        }
        marqueurFin -= (15-j);
        
        matrice.setStringCompressed(string.substring(marqueurTableFin + comp, marqueurFin));
        return matrice;

    }

    /**
     * Cette méthode permet de controler si la configuration est correct
     *
     * @return true si elle l'est, false sinon
     */
    public boolean getConfiguration() {
        return ControlAndCorrection(data);
    }

    /**
     * Cette méthode permet de controler et de corriger une matrice si beosoin
     * est
     *
     * @param data matrice de boolean
     * @return true si il n'y à plus d'erreur, false sinon
     */
    public static boolean ControlAndCorrection(boolean[][] data) {
        // Check lines
        int count1 = 0;
        int erreur = 0;
        int positionX = 0;
        int positionY = 0;
        for (int j = 0; j < data[0].length; j++) {
            count1 = 0;
            for (int i = 0; i < data.length; i++) {
                if (data[i][j] == true) {
                    count1++;
                }
            }
            if (count1 % 2 != 0) {
                erreur++;
                positionX = j;
            }
        }

        // Check colum
        for (int i = 0; i < data.length; i++) {
            count1 = 0;
            for (int j = 0; j < data[0].length; j++) {
                if (data[i][j] == true) {
                    count1++;
                }
            }
            if (count1 % 2 != 0) {
                erreur++;
                positionY = i;
            }
        }
        if (erreur == 2) {
            data[positionY][positionX] = reverseBoolean(data[positionY][positionX]);
            erreur = 0;
        }
        if (erreur > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Cette metode permet d'inverser un boolean
     *
     * @param bool boolean
     * @return la valeure du boolean inversé
     */
    public static boolean reverseBoolean(boolean bool) {
        if (bool) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Cette methode permet de retourner une matrice sous forme de string
     *
     * @param translated matrice de boolean
     * @return un string contenant le tableau de boolean
     */
    public static String toString(boolean[][] translated) {
        String StringCompressed = "";

        for (int x = 1; x < translated.length; x++) {
            for (int y = 1; y < translated.length; y++) {
                if (x == 1 && y == 1) {
                    y = 11;
                }
                if (translated[x][y]) {
                    StringCompressed += "1";
                } else {
                    StringCompressed += "0";
                }
            }
        }
        return StringCompressed;
    }

    /**
     * Cette méthode permet de retourner la configuration de la compression sous
     * forme de string
     *
     * @param data matrice de boolean
     * @return un string contenant la configuration
     */
    public static String getConfigurationCompression(boolean[][] data) {
        String Marquer = "";
        String ConfigCompress = "";
        for (int y = 1; !Marquer.equals("11111111"); y++) {
            for (int x = 1; x < data.length; x++) {
                if (data[y][x]) {
                    Marquer += "1";
                    ConfigCompress += "1";
                } else {
                    Marquer = "";
                    ConfigCompress += "0";
                }
                if (Marquer.equals("11111111")) {
                    break;
                }

            }

        }

        return ConfigCompress.substring(16, (ConfigCompress.length() - 8));
    }
}
